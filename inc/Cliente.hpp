#ifndef CLIENTE_HPP // arquivo de cabeçalho 
#define CLIENTE_HPP // Biblioteca

class Clientes{
    private:
    
        string nome ;
        long numero; 
        long cpf;
   
    public: 
        Clientes();

        string get_nome();
        void set_nome(string nome);

        long get_numero();
        void set_numero(long numero);

        long get_cpf();
        void set_cpf(long cpf);


}

#endif 
